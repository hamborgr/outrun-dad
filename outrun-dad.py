# made by hamborgr & sw1tchbl4d3 (https://sw1tchbl4d3.com)
# GLHF


from pathlib import Path 
import json
from random import randint
from colorama import init 
import subprocess
import sys

init()

def pad(string, num):
    return string + (num - len(string)) * ' '

class bcolors:
    HEADER = '\033[95m' #neon purple
    OKBLUE = '\033[94m' #blue
    OKGREEN = '\033[92m' #green
    WARNING = '\033[93m' #yellow
    FAIL = '\033[91m' #red
    ENDC = '\033[0m' #resets color
    BOLD = '\033[1m' #thickens text
    UNDERLINE = '\033[4m' #underlines font
    CLEAR = '\x1b[2J\x1b[H' #clears terminal (to work with Linux & Windows)

while True:
    print(bcolors.CLEAR)

    try:
        fd = open("highscores.json", "r")
        data = json.loads(fd.read())
    except FileNotFoundError:
        data = {}
    
    sorted_data = {k: v for k, v in reversed(sorted(data.items(), key=lambda item: item[1]))}
    
    j = 1
    for k in sorted_data:
        if (j < 6):
            print(pad(str(j) + '.', 3), pad(k, 20), " ~", sorted_data[k], "XP")
            j += 1

    print(bcolors.HEADER)
    input(' < Press Enter to Start >\n\n')
    print(bcolors.ENDC)

    print(bcolors.CLEAR)
    print('One day you are lying in bed and you are reading your favorite yaoi harem manga until your dad suddenly kicks down your door')
    print('You can only think of one thing in that moment....\n')
    print('Dad found the poop sock!\n')
    print('Your dad starts yelling and asks what your shit is doing in his favorite sock')
    print('You start screeching and dash out of the room just as he starts pulling out his leather belt')
    safe = True
    doom = False
    secret = False
    score = 0

    while safe:
        death_portal = randint(1, 5)
        granny_portal = randint(1, 5)
        bro_portal = randint(1, 5)
        doom_portal = 666
        secret_portal = 0
        score += 1
        print('____________________\n')
        print('There are suddenly five portals infront of you')
        print('Behind one of them certain DEATH awaits you.')
        print('Choose wisely...\n')

        print('1, 2, 3, 4 or 5? \n\n')
        print(bcolors.HEADER)
        portal = input(' > ')
        print(bcolors.ENDC)
    
        portal_num = int(portal)
        print(bcolors.CLEAR)

        if portal_num == doom_portal:
            doom = True
            safe = False
            print(bcolors.CLEAR)
            print(bcolors.OKGREEN,'> ACHIEVEMENT UNLOCKED: DOOM SLAYER\n',bcolors.ENDC)
            print('You suddenly get transported into another room')
            print('You look around')
            print('You seem have stumbled onto a forbidden demonic ritual\n')
            score += 100
            print('You have now obtained unearthly powers and superhuman strength')
            print('Just as you turn around you can see you dad turn into a demon')
            print('He sprints towards you')
            print('You grab him and start ripping and tearing with your bare hands...\n')
        elif portal_num == death_portal:
            safe = False
            doom = False
        elif portal_num == granny_portal:
            print('On the other side of the portal you can see your grandma\n')
            score += 69
            print('She tells you happy birthday and gives you a present')
            print('Then she sends you on your way and wishes you good luck')
            print('\n')
        elif portal_num == bro_portal:
            print('You seem to have wandered into your brothers domain')
            print('You seem to have angered him by interupting his hanime session')
            print('He takes away all of your beloved chicken tendies for trespassing and pushes you into a different direction')
            score -= 69
            print('  ')
        elif portal_num == secret_portal:
            print(bcolors.OKGREEN,'> ACHIEVEMENT UNLOCKED: ADVENTURER\n',bcolors.ENDC)
            print('You seem to have found a hidden hallway')
            print('The dark hallway happens to lead to two more portals...')
            score += 100
            print('There is a',bcolors.FAIL,'red',bcolors.ENDC,'(1) and a',bcolors.OKBLUE,'blue',bcolors.ENDC,'(2) portal')
            print(bcolors.HEADER)
            choice = input(' > ')
            print(bcolors.ENDC)
            nice_portal = randint(1, 2)
            notsonice_portal = randint(1, 2)

            if int(choice) == nice_portal:
                print(bcolors.CLEAR)
                print('You enter the chosen portal')
                print('You now end up in a new room\n')
            else:
                safe = False
        elif portal_num >666:
            print('There isnt such a portal...\n')
            score -= 1
        elif portal_num >5:
            print('There isnt such a portal...\n')
            score -= 1
        elif portal_num <0:
            print('There isnt such a portal...\n')
            score -= 1
        else:
            print('You find your mom in the next room')
            print('It seems she doesnt know what is going on yet')
            print('She also hasnt acknowledged you since she is busy looking at minion memes on facebook')
            score +=10
            print('you walk past her into the other portal wondering what awaits you on the other side\n')
            print('  ')


    if doom:
        print('CONCLUSION:',bcolors.OKBLUE,'YOU SURVIVED\n',bcolors.ENDC)
        print(bcolors.HEADER,'  ___',score,'XP ___\n',bcolors.ENDC)
        print(bcolors.OKGREEN,'> GG WP',bcolors.ENDC)
    else:
        print(bcolors.CLEAR)
        print('Just as you decide on a portal to enter, you feel someone put their hand on your shoulder')
        print('You turn around just in time to see your dad swing his belt')
        print('...\n...\n...\n')
        print('CONCLUSION:',bcolors.FAIL,'YOU DIED\n',bcolors.ENDC)
        score -= 1
        print(bcolors.HEADER,'  ___',score,'XP ___\n',bcolors.ENDC)
        print(bcolors.OKGREEN,'> GG EZ',bcolors.ENDC)


    if score == 420:
        print(bcolors.OKGREEN,'> BLAZE IT!\n',bcolors.ENDC)
    elif score == 69:
        print(bcolors.OKGREEN,'\n> ( ͡° ͜ʖ ͡°)\n',bcolors.ENDC)
    elif score == 0:
        print(bcolors.OKGREEN,'\n > PEASANT LMAO\n',bcolors.ENDC)
    else:
        print('')


    while True:
        print('Please Enter your Username:\n')
        print(bcolors.HEADER)
        name = input(' > ') 
        print(bcolors.ENDC)
        if len(name) > 20:
            print(bcolors.CLEAR)
            print('\nlöng\n')
        elif len(name) < 1:
            print(bcolors.CLEAR)
            print('\nshört\n')
        else:
            break
    data[name] = score

    with open("highscores.json", "w") as fd:
        fd.write(json.dumps(data))
